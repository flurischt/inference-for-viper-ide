'use strict';
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from 'vscode';
import child_process = require('child_process');

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {

    let diagnosticCollection = vscode.languages.createDiagnosticCollection('inference-for-viper'); 

    // reset the state of diagnostics upon the following events
    vscode.workspace.onDidChangeTextDocument(() => { diagnosticCollection.clear() });
    vscode.workspace.onDidCloseTextDocument(() => { diagnosticCollection.clear() });
    vscode.workspace.onDidChangeConfiguration(() => { diagnosticCollection.clear() });

    // Use the console to output diagnostic information (console.log) and errors (console.error)
    // This line of code will only be executed once when your extension is activated
    console.log('Extension "inference-for-viper" is now active!');

    // The command has been defined in the package.json file
    // Now provide the implementation of the command with  registerCommand
    // The commandId parameter must match the command field in package.json
    let disposable = vscode.commands.registerCommand('extension.runInfer', () => {
        // The code you place here will be executed every time your command is executed

        diagnosticCollection.clear(); 

        console.log('Inference for Viper is starting up...'); 

        if (vscode.window.activeTextEditor) { 
            let uri = vscode.window.activeTextEditor.document.uri;

            let document = vscode.window.activeTextEditor.document
            let fname: string = document.fileName
            let jpath: string = vscode.workspace.getConfiguration('inference-for-viper').get("path");
            let command: string = 'java -jar ' + jpath + ' ' + fname

            console.log('Executing command: `' + command + '`');
            return new Promise((resolve, reject) => { 
                let process = child_process.exec(command, { maxBuffer: 1024 * 4 });
                process.stdout.on('data', (data: string) => {
                    console.log('[SAMPLE] ' + data);

                    process.removeAllListeners();
                    process.stdout.removeAllListeners();
                    process.stderr.removeAllListeners();

                    let response = JSON.parse(data)

                    if ( response.type == InferenceMessage.OK ) {
                        let preconditions = response.preconditions; 
                        let postconditions = response.postconditions; 
                        let invariants = response.invariants; 

                        let edits = 
                            collect_edits(preconditions).concat( 
                            collect_edits(postconditions)).concat( 
                            collect_edits(invariants)); 
                        
                        let ws_edit = new vscode.WorkspaceEdit(); 
                        ws_edit.set(document.uri, edits); 
                        vscode.workspace.applyEdit(ws_edit); 

                        resolve(true)

                    } if ( response.type == InferenceMessage.ERROR ) { 
                        let errors = response.errors
                        let diagnostics: vscode.Diagnostic[] = [];

                        errors.forEach(err => {
                            let start = parse_pos(err.start);
                            let end = parse_pos(err.end);
                            let tag = err.tag;
                            let message = err.message;

                            let lineRange = new vscode.Range(start, end);
                            let diag = new vscode.Diagnostic(lineRange, message, vscode.DiagnosticSeverity.Information); 

                            diagnostics.push(diag); 
                        });

                        diagnosticCollection.set(document.uri, diagnostics);
                        
                        resolve(false)
                        
                    } else {
                        reject("parseFailed: message type `" + response.type + "` is not supported."); 
                    }
                });
                let errorReason = "";
                process.stderr.on('data',(data: string) => {
                    errorReason = errorReason += "\n" + data;
                });
                process.on("exit",code => {
                    reject("startupFailed: " + errorReason);
                });
            });

        } else {
            console.log('No active text editor found.');
            vscode.window.showInformationMessage('Please open the Viper program for which the specifications should be inferred.');
        }

        // Display a message box to the user
        console.log('Inference for Viper completed.');
    });

    context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {
    //diagnosticCollection.clear();
}

// auxiliary methods for inserting inferred specifications

enum InferenceMessage {
    OK = "SpecificationInference", 
    ERROR = "Error"
}

function collect_edits(additions: [ { position:string, specifications:[string] } ]): vscode.TextEdit[] {
    let edits: vscode.TextEdit[] = []
    additions.forEach(add => { 
        let pos = parse_pos(add.position); 
        let specs = add.specifications; 
        let indent_pattern = /^(\s*)(.*)$/g;
        let prefix = vscode.window.activeTextEditor.document.getText(new vscode.Range(new vscode.Position(pos.line,0),pos));
        let match = indent_pattern.exec(prefix)
        let indent = match[1] + '\t'
        let is_line_clear_to_the_left = match[2].length == 0 
        let newline_maybe = is_line_clear_to_the_left ? '' : '\n'
        let additional_tabs = is_line_clear_to_the_left ? '\t' : indent
        edits.push(vscode.TextEdit.insert(pos, newline_maybe + additional_tabs + specs.join('\n'+indent) + '\n' + match[1])); 
    });
    return edits
}

function parse_pos(pos: string): vscode.Position {
    let pattern = /^(\d+):(\d+)$/g;
    let line: number = -1; 
    let char: number = -1; 
    try {
        let match = pattern.exec(pos); 
        line = +match[1]; 
        char = +match[2]; 
    } catch (e) {
        console.log('Error parsing position `' + pos + '`: ' + e); 
    }
    return new vscode.Position(line-1, char-1); 
}

// this code has been partially borrowed from: 
// https://github.com/Microsoft/vscode-spell-check/blob/master/src/Features/spellProvider.ts
function triggerDiagnostics(document: vscode.TextDocument) {
    // Do nothing if the doc type is not one we should test
    if (vscode.workspace.getConfiguration('inference-for-viper').filetypes.indexOf(document.languageId) === -1) {
        return; 

    } else {
        
    }
}